import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv("final_dataset.csv")
fig, axs = plt.subplots(1, 2)
fig.subplots_adjust(top=0.8)
fig.suptitle("Average Points Over Match Weeks", size=20)
axs[0].plot(df.MW, df.HTP, 'o', markersize=2)
m, b = np.polyfit(df.MW, df.HTP, 1)
axs[0].plot(df.MW, m * df.MW + b)
axs[0].set_title("Home Teams", size=15)

axs[1].plot(df.MW, df.ATP, 'o', color='orange', markersize=2)
i, j = np.polyfit(df.MW, df.ATP, 1)
axs[1].plot(df.MW, i * df.MW + j)
axs[1].set_title("Away Teams", size=15)

for ax in axs.flat:
    ax.set(xlabel="Match Weeks", ylabel="Average Points Per Game")
for ax in axs.flat:
    ax.label_outer()
plt.show()

# Notice the slow improvement of the Home Team Average Points over the course of the league
# We want teams in the best shape, so we're going to get rid of the beginning teams.
