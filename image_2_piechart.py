import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv("final_dataset.csv")

df = df[df.MW > 10]
df = df[["HomeTeam", "AwayTeam", "FTHG", "FTAG", "FTR", "HTP", "ATP", "HM1", "HM2", "HM3", "AM1", "AM2", "AM3", "HTGD",
         "ATGD", "DiffPts", "DiffFormPts", "MW"]]

# I wanted to see if there was a home field advantage. If the home team really is most-likely to win, would make
# testing easier as I would just focus on home team.

number_matches = len(df.index)  # total matches played
homewins = len(df[df.FTR == "H"])  # Total home wins
homeloss = len(df[df.FTR == "NH"])  # Total ties/losses since we our data just specifies "Not Home"
winrate = (homewins / number_matches)  # simple math
lossrate = (homeloss / number_matches)

objects = ["Home Team", "Away Team"]
x_pos = np.arange(len(objects))
values = [np.mean(df.HTGD), np.mean(df.ATGD)]

# Create Pie Chart/Bar Chart
labels = "Home Team Wins", "Home Team Losses/Ties"
sizes = [winrate * 100, lossrate * 100]
fig, axs = plt.subplots(2, 1, figsize=(10, 10))
fig.suptitle("Is There Really Home Field Advantage?", size=25)
axs[0].pie(sizes, explode=(0, 0.1), labels=labels, autopct='%1.1f%%', shadow=True, startangle=90)
axs[0].set_title("Home Team Win Ratio", size=20)

axs[1].bar(x_pos, values, capsize=5, align='center', color=["blue", "orange"])

plt.xticks(x_pos, objects, fontsize=15)
axs[1].set_title("Average Goal Difference Per Game", size=20)
axs[1].set_ylabel("AVG Goal Difference", size=15)
plt.show()
# So from this we can gather, home team does not have home field advantage.
