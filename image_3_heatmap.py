import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

df = pd.read_csv("final_dataset.csv")
df = df[df.MW > 10]
df = df[["HomeTeam", "AwayTeam", "FTHG", "FTAG", "FTR", "HTP", "ATP", "HM1", "HM2", "HM3", "AM1", "AM2", "AM3", "HTGD",
         "ATGD", "DiffPts", "DiffFormPts", "MW"]]

# Want to see what correlations we have to have proper independent variables for Logistic Regression.
plt.subplots(figsize=(12, 6))
ax = sns.heatmap(df.corr(), annot=True)
ax.set_title("Correlation Heat Map", size=25)
plt.show()

# We have six correlations above .60
# HTP (Home Team Average Points), ATP(Away Team Average Points), HTGD(Home Team Goal Difference),
# ATGD(Away Team Goal Difference), DiffPts(The Difference in Points After Game),DiffFormPts(Differences in Form Points)
